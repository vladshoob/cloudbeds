<?php

namespace App\Controllers;

use App\Price;
use App\Requests\PriceRequest;
use Carbon\Carbon;

class PricesController
{
    /**
     * Return a main page with prices table.
     */
    public function index()
    {
        // get sorted prices
        $prices = Price::orderBy('date_start')->get();

        // present them in view
        require '../views/index.php';
    }

    /**
     * Store a new price to DB.
     * Makes a redirect back.
     */
    public function store()
    {
        // cast values to appropriate types
        $date_start = (string) $_POST['date_start'];
        $date_end = (string) $_POST['date_end'];
        $price = (float) $_POST['price'];

        // store, if all is set
        if ($date_start && $date_end && $price) {
            $start = Carbon::createFromFormat('Y-m-d', $date_start)->startOfDay();
            $end = Carbon::createFromFormat('Y-m-d', $date_end)->startOfDay();

            (new PriceRequest($start, $end, $price))->apply();

//            // get all affected prices
//            $prices = Price::affected($start, $end, $price)->get();
//
//            // 1 - if there are no affected intervals, just create new one
//            if ($prices->isEmpty()) {
//                Price::create(['date_start' => $date_start, 'date_end' => $date_end, 'price' => $price]);
//
//            // in other case we need to check each
//            } else {
//                foreach ($prices as $key => $model) {
//                    // update exact
//                    if ($model->date_start->equalTo($start) && $model->date_end->equalTo($end)) {
//                        if ($price != $model->price) {
//                            // TODO: check whether we need to merge
//                            $model->update(['date_start' => $date_start, 'date_end' => $date_end, 'price' => $price]);
//                        }
//                    }
//
//                    // one of edges
//                    else {
//                        // left edge
//                        if ($model->date_end->equalTo((clone $start)->subDay()) && $model->price == $price) {
//                            $model->update(['date_end' => $date_end]);
//
//                        // right edge
//                        } elseif ($model->date_start->equalTo((clone $end)->addDay()) && $model->price == $price) {
//                            $model->update(['date_start' => $date_start]);
//                        }
//                    }
//                }
//            }
        }

        $this->back();
    }

    /**
     * Update a price.
     */
    public function update()
    {
        // cast values to appropriate types
        $date_start = (string) $_POST['date_start'];
        $date_end = (string) $_POST['date_end'];
        $price = (float) $_POST['price'];

        // update, if all is set
        if ($date_start && $date_end && $price) {
            Price::where('id', (int) $_POST['id'])->update([
                'date_start' => $date_start,
                'date_end' => $date_end,
                'price' => $price
            ]);
        }

        $this->back();
    }

    /**
     * Destroy particular or all prices.
     */
    public function destroy()
    {
        // delete all, if special type is given
        if ($_POST['type'] ?? Null == 'all')
            Price::query()->delete();

        // delete single
        if ($_POST['id']) {
            Price::where('id', $_POST['id'])->delete();
        }

        $this->back();
    }

    /**
     * Redirect to avoid multiple form submissions.
     */
    protected function back() {
        global $base_url;
        header("Location: " . $base_url);
        exit();
    }
}