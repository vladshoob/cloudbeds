<?php

namespace App\Requests;

use App\Price;
use Carbon\Carbon;

class PriceRequest
{
    protected $start;
    protected $end;
    protected $price;
    protected $affected;

    public function __construct(Carbon $start, Carbon $end, float $price)
    {
        $this->start = $start;
        $this->end = $end;
        $this->price = $price;

        $this->affected = $prices = Price::affected($start, $end, $price)->get();
    }

    public function apply()
    {
        // new one
        if (!count($this->affected)) {
            Price::create(['date_start' => $this->start, 'date_end' => $this->end, 'price' => $this->price]);

        // there might be some affected
        } else {

            // update exact, but check its edges
            if ($model = $this->affected->where('date_start', $this->start)->where('date_end', $this->end)->first()) {

                // check for left edge
                if ($left = $this->affected->where('date_end', (clone $this->start)->subDay())->where('price', $this->price)->first()) {
                    $this->start = $left->date_start;
                    $left->delete();
                }

                // check right edge
                if ($right = $this->affected->where('date_start', (clone $this->end)->addDay())->where('price', $this->price)->first()) {
                    $this->end = $right->date_end;
                    $right->delete();
                }

                // update
                $model->update(['date_start' => $this->start, 'date_end' => $this->end, 'price' => $this->price]);
            }

            // merges and divisions
            elseif ($collection = $this->affected->where('date_end', '>=', $this->start)->where('date_start', '<=', $this->end)) {

                foreach ($collection as $model) {
                    // if type is outside left
                        // if same price
                        // if another price

                    // if type is outside right
                        // if same price
                        // if another price

                    // if type is within
                        // 

                    // if type is cover
                }
                var_dump($collection); die;
//                // single merge overlap
//                if ($collection->count() == 1 && $this->price == ($model =  $collection->first())->price) {
//                    $model->update([
//                        'date_start' => $model->date_start < $this->start ? $model->date_start : $this->start,
//                        'date_end' => $model->date_end > $this->end ? $model->date_end : $this->end,
//                        'price' => $this->price]);
//                }
//
//                // single division overlap
//                elseif ($collection->count() == 1 && $this->price != ($model =  $collection->first())->price) {
//                    // cut old
//                    var_dump($this->end, $model->date_start, $this->end > $model->date_start);
//                    var_dump($this->start, $model->date_end, $this->end > $model->date_end); die;
//                    $is_left_cross = $this->end > $model->date_start && $this->start < $model->date_end;
//                    $model->update([
//                        'date_start' => $is_left_cross ? (clone $this->end)->addDay() : $model->date_start,
//                        'date_end' => $is_left_cross ? (clone $this->start)->subDay() : $this->date_end
//                    ]);
//
//                    // create separate
//                    Price::create(['date_start' => $this->start, 'date_end' => $this->end, 'price' => $this->price]);
//
//                // many to resolve
//                } else {
//
//                }
            }

            // new, but there might be collision on outside edges
            else {
                // check for left edge
                if ($left = $this->affected->where('date_end', (clone $this->start)->subDay())->where('price', $this->price)->first()) {
                    $this->start = $left->date_start;
                    $model_for_update = $left;
                }

                // check right edge
                // delete it if model for update is already set
                if ($right = $this->affected->where('date_start', (clone $this->end)->addDay())->where('price', $this->price)->first()) {
                    $this->end = $right->date_end;
                    if (isset($model_for_update)) {
                        $right->delete();
                    } else $model_for_update = $right;
                }

                // create new or use existing edge for update
                $model = $model_for_update ?? new Price();
                $model->date_start = $this->start;
                $model->date_end = $this->end;
                $model->price = $this->price;
                $model->save();
            }

        }


    }
}