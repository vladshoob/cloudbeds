<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    /**
     * Do not apply mass assignment exception.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Cast columns to appropriate data types.
     *
     * @var array
     */
    protected $casts = [
        'date_start' => 'date',
        'date_end' => 'date',
    ];

    /**
     * Query all affected intervals.
     *
     * @param $query
     * @param Carbon $start
     * @param Carbon $end
     * @param float $price
     * @return mixed
     */
    public function scopeAffected($query, Carbon $start, Carbon $end, float  $price)
    {
        $start = clone $start;
        $end = clone $end;

        return $query
            // one that ends a day earlier from given range, and needs to be merged
            ->where(function ($query) use ($start, $price) {
                $query->whereDate('date_end', $start->subDay())->where('price', $price);
            })

            // one that start day after given range, and needs to be merged
            ->orWhere(function ($query) use ($end, $price) {
                $query->whereDate('date_start', $end->addDay())->where('price', $price);
            })

            // overlapping ones
            ->orWhere(function ($query) use ($start, $end) {
                $query->where('date_start', '<=', $end)->where('date_end', '>=', $start);
            });
    }
}