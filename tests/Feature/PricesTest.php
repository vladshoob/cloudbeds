<?php

namespace Tests\Feature;

use App\Price;
use GuzzleHttp\Client;
use Tests\TestCase;

class PricesTest extends TestCase
{
    /**
     * Prepare GUZZLE client instance.
     *
     * @var
     */
    protected $client;

    /**
     * Assign GUZZLE client.
     *
     * @return void
     * @throws \Exception
     */
    public function setUp() :void
    {
        parent::setUp();

        $this->client = new Client;
    }

    /**
     * Checks that home page is accessible.
     *
     * @test
     */
    public function ensure_prices_page_is_accessible()
    {
        // make a request to receive response
        $response = $this->client->get($this->base_url);

        // check status code
        $this->assertEquals(200, $response->getStatusCode());

        // check visibility of html
        $this->assertStringContainsString('Vladyslav Shubionkin', $response->getBody()->read(2048));
    }

    /** @test */
    public function new_price_form_can_be_submitted()
    {
        // make a post request
        $response = $this->client->post("{$this->base_url}/prices", [
            'date_start' => '2019-03-24',
            'date_end' => '2019-03-24',
            'price' => 12.34,
        ]);

        // check response code
        $this->assertEquals(302, $response->getStatusCode());

        // assert created
        $this->assertCount(1, Price::all());

        $this->clearDatabase();
    }

    /** @test */
    public function update_price_form_can_be_submitted()
    {
        Price::create(['date_start' => '2134-01-02', 'date_end' => '2134-01-02', 'price' => 12.34]);

        // make a post request
        $response = $this->client->patch("{$this->base_url}/prices/1", [
            'date_start' => '2019-03-25',
            'date_end' => '2019-03-25',
            'price' => 12.35,
        ]);

        // check response code
        $this->assertEquals(302, $response->getStatusCode());

        // assert created
        $this->assertCount(1, Price::where('price', 12.35));

        $this->clearDatabase();
    }

    /** @test */
    public function delete_all_prices_form_can_be_submitted()
    {
        // create a world
        Price::create(['date_start' => '2134-01-02', 'date_end' => '2134-01-02', 'price' => 12.34]);
        Price::create(['date_start' => '2134-01-02', 'date_end' => '2134-01-02', 'price' => 12.34]);

        // sanity check
        $this->assertCount(2, Price::all());

        // make a delete request with special type
        $response = $this->client->delete("{$this->base_url}/prices", ['type' => 'all']);

        // check response code
        $this->assertEquals(302, $response->getStatusCode());

        // assert all prices deleted
        $this->assertCount(0, Price::all());
    }

    /** @test */
    public function delete_one_price_form_can_be_submitted()
    {
        // create a world
        Price::create(['date_start' => '2134-01-02', 'date_end' => '2134-01-02', 'price' => 12.34]);
        Price::create(['date_start' => '2134-01-02', 'date_end' => '2134-01-02', 'price' => 12.34]);

        // sanity check
        $this->assertCount(2, Price::all());

        // make a delete request with special type
        $response = $this->client->delete("{$this->base_url}/prices/1", [
            'id' => 1
        ]);

        // check response code
        $this->assertEquals(302, $response->getStatusCode());

        // assert all price is deleted
        $this->assertCount(1, Price::where('id', 2)->get());
    }
}
