<?php

namespace Tests;

use App\Price;
use PHPUnit\Framework\TestCase as BaseTestCase;

class TestCase extends BaseTestCase
{
    protected $base_url;

    /**
     * Set up an application.
     *
     * @return void
     * @throws \Exception
     */
    public function setUp() :void
    {
        parent::setUp();

        // require composer autoload files
        require __DIR__ . '/../vendor/autoload.php';

        // connect to DB
        require_once __DIR__ . '/../config/database.php';

        // some general settings
        require_once __DIR__ . '/../config/settings.php';
        $this->base_url = $base_url;

        $this->clearDatabase();
    }

    /**
     * Clear DB for test purposes.
     *
     * @return void
     */
    protected function clearDatabase() :void {
        Price::query()->delete();
    }
}