<?php

namespace Tests\Feature;

use App\Price;
use Tests\TestCase;

class PriceTest extends TestCase
{
    /** @test */
    public function a_price_can_be_created_in_database()
    {
        // store to DB
        Price::create([
            'date_start' => '2134-01-02',
            'date_end' => '2134-01-02',
            'price' => 12.34
        ]);

        // assert created
        $this->assertCount(1, Price::all());

        $this->clearDatabase();
    }
}