<?php

// require composer autoload files
require __DIR__ . '/../vendor/autoload.php';

// connect to DB
require_once __DIR__ . '/../config/database.php';

// some general settings
require_once __DIR__ . '/../config/settings.php';

// mini router
// refactor note: dedicated component required, if not test app
use App\Controllers\PricesController;

if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    // delete
    if (($_POST['_method'] ?? Null) === 'DELETE') {
        (new PricesController())->destroy();
    }

    // delete
    if (($_POST['_method'] ?? Null) === 'PATCH') {
        (new PricesController())->update();
    }

    // store
    (new PricesController())->store();

} else {
    // index
    (new PricesController())->index();
}
