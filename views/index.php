<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Cloudbeds test</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- Font Awesome -->
    <script defer src="https://use.fontawesome.com/releases/v5.8.1/js/solid.js" integrity="sha384-IA6YnujJIO+z1m4NKyAGvZ9Wmxrd4Px8WFqhFcgRmwLaJaiwijYgApVpo1MV8p77" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.8.1/js/fontawesome.js" integrity="sha384-EMmnH+Njn8umuoSMZ3Ae3bC9hDknHKOWL2e9WJD/cN6XLeAN7tr5ZQ0Hx5HDHtkS" crossorigin="anonymous"></script>
    <!-- Favicon -->
    <link rel="icon" href="https://www.cloudbeds.com/wp-content/uploads/2018/08/favmaster_tiny.png" sizes="32x32" />
    <link rel="icon" href="https://www.cloudbeds.com/wp-content/uploads/2018/08/favmaster_tiny.png" sizes="192x192" />
</head>

<body class="bg-light">

    <div class="container">

        <!-- HEADING -->
        <div class="py-5 text-center">
            <img class="d-block mx-auto mb-4" src="https://www.adshotel.com/wp-content/uploads/2018/03/CB_logo_2clr_lrg-1.png" alt="Cloudbeds" height="72">
            <h2>Test task</h2>
            <p class="lead text-muted font-italic">
                Applicant: <a href="https://www.linkedin.com/in/vladyslav-shubionkin-05676993/">Vladyslav Shubionkin</a>
            </p>
        </div>

        <div class="d-flex mb-3">
            <div class="flex-fill">
                <form action="/prices" method="POST">
                    <div class="input-group">
                        <input type="date" class="form-control" placeholder="Start Date" name="date_start" required>
                        <input type="date" class="form-control" placeholder="End Date" name="date_end" required>
                        <input type="number" step="0.01" class="form-control" placeholder="Price" name="price" required>
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="submit">Save</button>
                        </div>
                    </div>
                </form>
            </div>

            <div class="pl-5">
                <form action="/prices" method="POST">
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="hidden" name="type" value="all">

                    <button type="submit" class="btn btn-danger">Clear all</button>
                </form>
            </div>
        </div>

        <?php require '_table.php'; ?>

        <footer class="my-5 pt-5 text-muted text-center text-small">
            <p class="mb-1">
                Images are owned by <a href="https://www.cloudbeds.com">cloudbeds.com</a>.
            </p>
        </footer>
    </div>

    <!-- Core JS-->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</body>
</html>
