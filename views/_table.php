<div class="card">
    <div class="card-header">Prices (sorted by start date)</div>

    <?php if ($prices->isEmpty()): ?>

        <div class="card-body text-center">
            Prices table is empty.
        </div>

    <?php else: ?>

        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Start Date</th>
                    <th scope="col">Date End</th>
                    <th scope="col">Price</th>
                    <th scope="col" width="10%">Created At</th>
                    <th scope="col" width="10%">Updated At</th>
                    <th scope="col" width="10%" class="text-center">Actions</th>
                </tr>
                </thead>

                <tbody>
                    <?php foreach ($prices as $price): ?>

                        <tr class="d-none">
                            <td>
                                <form action="/prices/<?= $price->id ?>" method="POST" id="price-<?= $price->id ?>" class="d-none">
                                    <input type="hidden" name="_method" value="PATCH">
                                    <input type="hidden" name="id" value="<?= $price->id ?>">
                                    <input type="hidden" name="date_start" value="<?= $price->date_start ?>">
                                    <input type="hidden" name="date_end" value="<?= $price->date_end ?>">
                                    <input type="hidden" name="price" value="<?= $price->price ?>">
                                </form>
                            </td>
                        </tr>

                        <tr>
                            <th scope="row"><?= $price->id ?></th>
                            <td>
                                <input type="date"
                                       class="form-control"
                                       value="<?= $price->date_start->format('Y-m-d') ?>"
                                       onchange="$('#price-<?= $price->id ?> input[name=date_start]').val(this.value)">
                            </td>
                            <td>
                                <input type="date"
                                       class="form-control"
                                       value="<?= $price->date_end->format('Y-m-d') ?>"
                                       onchange="$('#price-<?= $price->id ?> input[name=date_end]').val(this.value)">
                            </td>
                            <td>
                                <input type="number"
                                       step="0.01"
                                       class="form-control"
                                       value="<?= $price->price ?>"
                                       onchange="$('#price-<?= $price->id ?> input[name=price]').val(this.value)">
                            </td>
                            <td><?= $price->created_at->format('d.m.Y H:i:s') ?></td>
                            <td><?= $price->updated_at->format('d.m.Y H:i:s') ?></td>
                            <td class="text-center d-flex justify-content-center">
                                <button type="button"
                                        class="btn btn-sm btn-success mr-1"
                                        title="Save"
                                        onclick="$('#price-<?= $price->id ?>').submit()">
                                    <i class="fas fa-edit"></i>
                                </button>

                                <form action="/prices/<?= $price->id ?>" method="POST">
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="hidden" name="id" value="<?= $price->id ?>">
                                    <button type="submit" class="btn btn-sm btn-danger" title="Delete">
                                        <i class="fas fa-trash"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>



    <?php endif; ?>
</div>
